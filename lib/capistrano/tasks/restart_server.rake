namespace :server do
  desc 'run some rake db task'
  task :restart_unicorn do
    on roles(:app) do
      within "#{current_path}/client" do
        with rails_env: "#{fetch(:stage)}" do
          execute "sudo service unicorn_myblog reload "
        end
      end
    end
  end

  task :restart_nginx do
    on roles(:app) do
      within "#{current_path}/client" do
        with rails_env: "#{fetch(:stage)}" do
          execute "sudo service nginx restart"
        end
      end
    end
  end
end
after "ember:build", "server:restart_unicorn"
after "server:restart_unicorn", "server:restart_nginx"
