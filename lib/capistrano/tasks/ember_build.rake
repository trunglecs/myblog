namespace :ember do
  desc 'run some rake db task'
  task :build do
    on roles(:app) do
      within "#{current_path}/client" do
        with rails_env: "#{fetch(:stage)}" do
          execute "cd #{current_path}/client && npm i && bower i && ember build --environment=production"
        end
      end
    end
  end
end
after "deploy:published", "ember:build"
