// app/controllers/application.js
import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  renderNoTemplate: function(){
    Ember.$(document).resize();
    Ember.$('html,body').animate({scrollTop: 0}, 0, 'easeInOutExpo');

    var path = this.get('currentRouteName');
    return path === "login" || path === 'page-not-found' || path === 'error' || path === 'unauthorized';

  }.property('currentRouteName')
});
