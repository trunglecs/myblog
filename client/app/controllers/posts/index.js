import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  sortedList: Ember.computed.sort('model', 'sortDefinition'),
  sortDefinition: ['created_at:desc']
});
