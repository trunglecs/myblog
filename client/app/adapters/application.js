import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default DS.JSONAPIAdapter.extend(DataAdapterMixin, {
  namespace: 'api',
  authorizer: 'authorizer:devise',
  coalesceFindRequests: true,
  headers: Ember.computed('session.isAuthenticated', function() {
    const auth_data = this.get('session.data.authenticated');
    return {
      'access-token': auth_data['accessToken'],
      'token-type': auth_data['tokenType'],
      'uid': auth_data['uid'],
      'client': auth_data['client']
    };
  })
});
