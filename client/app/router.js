import Ember from 'ember';
import config from './config/environment';
import googlePageview from './mixins/google-pageview';

const Router = Ember.Router.extend(googlePageview, {
  location: config.locationType
});

Router.map(function() {
  this.route('home', {path: 'blog'});

  // about route
  this.route('about', {path: '/'})

  // tags route
  this.route('tags', {path: '/tags/:tag'});

  // post routes
  this.route('posts', function(){
    this.route('index', {path: '/'});
    this.route('show', {path: '/:id'});
    this.route('edit', {path: '/:id/edit'});
    this.route('new', {path: '/new'});
  });
  // category routes
  this.route('categories', function(){
    this.route('show', {path: '/:category'});
  });
  // login
  this.route('login', { path: '/login' });

  // error routes
  this.route('unauthorized', { path: '/unauthorized' });
  this.route('page-not-found', { path: '/*wildcard' });

});

export default Router;
