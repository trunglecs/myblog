import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  session: Ember.inject.service('session'),
  didRender: function() {
    Ember.run.scheduleOnce('afterRender', this, function() {
      _topNav();
    });
  },
  actions: {
    invalidateSession() {
      var session = this.get('session');
      var headers = {
        'access-token': session['accessToken'],
        'token-type': session['tokenType'],
        'uid': session['uid'],
        'client': session['client']
      };
      session.invalidate(headers);
    }
  }
});
