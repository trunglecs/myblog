import Ember from 'ember';

export default Ember.Component.extend({
  comment: {},
  store: Ember.inject.service(),
  actions: {
    post_comment(){
      let {comment, owner} = this.getProperties('comment', 'owner');
      let store = this.get('store');
      var new_comment = store.createRecord('comment', {
        author: comment.author,
        content: comment.content,
        post_id: owner.get('id'),
        post: owner
      });
      new_comment.save().then(function(response){
        console.log(response);
      });
      this.set('comment.content', "");
    }
  }
});
