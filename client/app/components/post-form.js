import Ember from 'ember';

export default Ember.Component.extend({
  contentHeight: 200,
  editingDisabled: false,
  actions: {
    saveItem(param, isPublished) {
      this.set('item.is_active', isPublished);
      this.sendAction('action', param);
    },
    reviewPost(state) {
      this.set('isReview', state);
      this.set('item.updated_at', Date.now() );
      jQuery('html,body').animate({scrollTop: 0}, 0, 'easeInOutExpo');
    },
    changeHeight(someObject) {
      var height = someObject.doSomeCalculationToGetHeight();
       this.set('contentHeight', height)
    }
  }
});
