import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  show_reply: false,
  reply: {},
  store: Ember.inject.service(),
  actions: {
    show_reply_box(){
      this.set('show_reply', true);
    },
    hide_reply_box(){
      this.set('show_reply', false);
      this.set('reply.content', '');
    },
    post_reply(){
      let reply = this.get('reply');
      let store = this.get('store');
      let comment = this.get('comment');
      console.log(comment.get('content'));
      var self = this;
      var new_reply = store.createRecord('comment', {
        author: reply.author,
        content: reply.content,
        parent_id: comment.id,
        parent: comment
      });
      new_reply.save().then(function(response){
        comment.get('replies').pushObject(new_reply);
        self.send('hide_reply_box');
      });
    }
  }
});
