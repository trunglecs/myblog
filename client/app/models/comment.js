import DS from 'ember-data';

export default DS.Model.extend({
  author: DS.attr(),
  content: DS.attr(),
  post_id: DS.attr(),
  parent_id: DS.attr(),
  created_at: DS.attr(),
  post: DS.belongsTo('post'),
  parent: DS.belongsTo('comment', { inverse: 'parent'}),
  replies: DS.hasMany('comment', { inverse: 'replies' })
});
