import DS from "ember-data";
export default DS.Model.extend({
  title: DS.attr('string'),
  abstract: DS.attr('string'),
  body: DS.attr('string'),
  tag_list: DS.attr('string'),
  topic_id: DS.attr('string'),
  category_id: DS.attr('string'),
  created_at: DS.attr('date'),
  updated_at: DS.attr('date'),
  is_active: DS.attr('boolean'),
  logo_tags: DS.attr('string'),
  tag_counts: DS.attr(),
  gallery_section: DS.attr('string'),
  // comments: DS.hasMany('comment', {async: true}),
  category: DS.belongsTo('category', {async: true})
});
