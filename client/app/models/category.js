import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  slug: DS.attr('string'),
  description: DS.attr('string'),
  posts_count: DS.attr('string'),
  posts: DS.hasMany('post', {async: true}),
});
