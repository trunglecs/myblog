import Ember from "ember";
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {
  model() {
    return Ember.RSVP.hash({
      categories: this.store.findAll("category")
    });
  },
  renderTemplate(controller, model){
    // render main outlet
    this.render();
    // render right content outlet
    this.render('categories-site-bar', {
      into: 'application',
      outlet: 'rightContent',
      model: model.categories
    });
  }
});
