import Ember from "ember";
export default Ember.Route.extend({
  model() {
    return this.store.findAll('post', {reload: true});
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('breadcrumbs', [
      {name: "Home"},
    ]);
  },
  renderTemplate(controller, model){
    this.render();
    this.render("page-header",{
      into: "application",
      outlet: "pageHeader"
    });
  }
});
