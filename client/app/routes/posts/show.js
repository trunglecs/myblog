import Ember from "ember";
export default Ember.Route.extend({
  model(params) {
    var self = this;
    return this.store.findRecord('post', params.id);
  },
  setupController(controller, model) {
    this._super(controller, model);
    Ember.getOwner(this).lookup('controller:application').set('layout', 'layouts/center_block');
    controller.set('breadcrumbs', [
      {name: "Home", route: 'home'},
      {name: model.get('category').get('name'), route: 'categories.show', param: model.get('category').get('slug')},
      {name: model.get('title')}
    ]);
  },
  renderTemplate(controller, model){
    this.render();
    this.render("page-header",{
      into: "application",
      outlet: "pageHeader"
    });
  },
  actions: {
    willTransition(){
      Ember.getOwner(this).lookup('controller:application').set('layout', null);
    },
    error: function(reason, transition) {
      this.transitionTo('/page-not-found', 'page-not-found');
    }
  }
});
