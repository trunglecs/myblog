import Ember from "ember";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  model() {
    return this.store.createRecord('post');
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('categories', this.store.peekAll('category'));
  },
  actions: {
    savePost(newPost){
      let self = this;
      newPost.save().then(function(response){
        let model = self.controller.get('model');
        if (model.get('is_active')) {
          _toastr("Post saved and Published","top-right","success",false);
          self.transitionTo('posts');
        } else {
          _toastr("Post saved as draft","top-right","primary",false);
          self.transitionTo('posts.edit', model.id);
        }
      });
    },
    willTransition(){
      let model = this.controller.get('model');
      if (model.get('isNew')){
        model.destroyRecord();
      }
    }
  }
});
