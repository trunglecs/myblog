import Ember from "ember";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  model(param) {
    return this.store.findRecord('post', param.id);
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('categories', this.store.peekAll('category'));
  },
  actions: {
    savePost(editPost){
      let self = this;
      editPost.save().then(function(response){
        let model = self.controller.get('model');
        if (model.get('is_active')) {
          _toastr("Post saved and Published","top-right","success",false);
          self.transitionTo('posts');
        } else {
          _toastr("Post saved as draft","top-right","primary",false);
          self.transitionTo('posts.edit', model.id);
        }
      });
    },
    willTransition(transition){
      let model = this.controller.get('model');
      if (model.get('hasDirtyAttributes')) {
        let confirmation = confirm("Your changes haven't saved yet. Would you like to leave this form?");

        if (confirmation) {
          model.rollbackAttributes();
        } else {
          transition.abort();
        }
      }
    }
  }
});
