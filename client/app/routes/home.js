import Ember from "ember";

export default Ember.Route.extend({
  model: function () {
    return this.store.findAll('post', {reload: true});
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('breadcrumbs', [
      {name: "Home"},
    ]);
  },
  renderTemplate(controller, model) {
    this.render('posts/index', {
      model: model
    });
    this.render("page-header",{
      into: "application",
      outlet: "pageHeader"
    });
  }
});
