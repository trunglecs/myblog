import Ember from 'ember';

export default Ember.Route.extend({
  model: function (params) {
    var self = this;
    return this.store.queryRecord('category', {id: params.category});
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('breadcrumbs', [
      {name: "Home", route: 'home'},
      {name: model.get('name')}
    ]);
  },
  renderTemplate(controller, model) {
    this.render('posts/index', {
      model: model.get('posts')
    });
    this.render("page-header",{
      into: "application",
      outlet: "pageHeader"
    });
  }
});
