import Ember from "ember";
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend({
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('breadcrumbs', [
      {name: "Blog", route: 'home'},
      {name: 'Trung Le'}
    ]);
    Ember.getOwner(this).lookup('controller:application').set('layout', 'layouts/center_block');
  },
  renderTemplate(controller, model){
    this.render();
    this.render("page-header",{
      into: "application",
      outlet: "pageHeader"
    });
  },
  actions: {
    willTransition(){
      Ember.getOwner(this).lookup('controller:application').set('layout', null);
    }
  }
});
