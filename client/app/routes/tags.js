import Ember from "ember";

export default Ember.Route.extend({
  model: function (param) {
    return this.store.queryRecord('post', {tag: param.tag});
  },
  renderTemplate(controller, model) {
    this.render('posts/index', {
      model: model
    });
  }
});
