import Ember from "ember";

export default Ember.Route.extend({
  setupController(controller, model) {
    this._super(controller, model);
    Ember.getOwner(this).lookup('controller:application').set('layout', 'layouts/blank');
  },
  actions: {
    willTransition(){
      Ember.getOwner(this).lookup('controller:application').set('layout', null);
    }
  }
});
