/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  // css imports
  app.import('bower_components/devicon/devicon.css');
  app.import('bower_components/devicon/devicon-colors.css');

  // javascript imports
  app.import('vendor/assets/plugins/bootstrap.js');
  app.import('vendor/assets/js/app_init.js');
  app.import('vendor/assets/plugins/toastr.js');
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
