# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
if Category.all.count == 0
  Category.create!(name: "Ruby & Rails")
  Category.create!(name: "Ember JS")
  Category.create!(name: "Setup & Deploy")
  Category.create!(name: "Tutorials & Ebooks")
end

if User.all.count == 0
  User.create!(email: "nmadtu@gmail.com", password: "12345678")
end
