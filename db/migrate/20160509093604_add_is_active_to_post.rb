class AddIsActiveToPost < ActiveRecord::Migration
  def change
    add_column :posts, :is_active, :boolean
  end
end
