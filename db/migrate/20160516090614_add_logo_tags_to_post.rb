class AddLogoTagsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :logo_tags, :text
  end
end
