class AddImageSectionToPost < ActiveRecord::Migration
  def change
    add_column :posts, :image_section, :text
  end
end
