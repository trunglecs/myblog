class RemoveImageSectionFromPost < ActiveRecord::Migration
  def change
    remove_column :posts, :image_section, :string
  end
end
