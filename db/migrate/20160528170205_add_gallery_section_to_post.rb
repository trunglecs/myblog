class AddGallerySectionToPost < ActiveRecord::Migration
  def change
    add_column :posts, :gallery_section, :text
  end
end
