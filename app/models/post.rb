class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  acts_as_taggable
  
  belongs_to :topic
  belongs_to :category
  has_many :comments

  scope :published, -> {where(is_active: true)}
end
