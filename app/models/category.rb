class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :topics
  has_many :posts, -> { where(is_active: true) }
end
