class CommentSerializer < ActiveModel::Serializer
  attributes :id, :author, :content, :post_id, :parent_id, :created_at
  has_many :replies, serializer: ReplySerializer
end
