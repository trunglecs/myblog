class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :abstract, :body, :tag_counts, :created_at, :topic_id, :category_id, :is_active, :logo_tags, :tag_list, :gallery_section

  def id
    object.slug
  end

  def tag_list
    object.tag_list.join(",")
  end
end
