class CategoriesSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :slug
  has_many :posts

  def attributes(*args)
    data = super
    data[:posts_count] = object.posts.size

    data
  end
end
