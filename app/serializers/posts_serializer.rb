class PostsSerializer < ActiveModel::Serializer
  attributes :id, :title, :abstract, :created_at, :topic_id, :category_id, :is_active, :logo_tags, :gallery_section
  def id
    object.slug
  end
end
