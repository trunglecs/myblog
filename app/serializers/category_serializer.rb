class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :slug
  has_many :posts

end
