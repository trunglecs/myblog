class ReplySerializer < ActiveModel::Serializer
  attributes :id, :author, :content, :post_id, :parent_id, :created_at
end
