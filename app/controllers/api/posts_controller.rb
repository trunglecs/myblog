class Api::PostsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_filter :get_post, only: [:show, :update, :destroy]

  def index
    @posts = current_user ? Post.all : Post.published
    @posts = @posts.tagged_with(params[:tag]) if params[:tag]

    render json: @posts, each_serializer: PostsSerializer
  end

  def show
    render json: @post
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render json: @post
    else
      render json: @post.errors
    end
  end

  def update
    if @post.update(post_params)
      render json: @post
    else
      render json: @post.errors
    end
  end

  def destroy
  end

  private

  def get_post
    @post = current_user ? Post.friendly.find(params[:id]) : Post.published.friendly.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: {}, status: 404
  end

  def post_params
    params.require(:data).require(:attributes).permit(:id, :title, :abstract, :body, :tag_list, :is_active, :category_id, :logo_tags ,:topic_id, :gallery_section)
  end

end
