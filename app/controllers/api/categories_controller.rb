class Api::CategoriesController < ApplicationController
  def index
    if params[:id]
      redirect_to api_category_url(params[:id])
    else
      @categories = Category.includes(:posts).all
      render json: @categories, each_serializer: CategoriesSerializer
    end
  end

  def show
    @category = Category.friendly.find(params[:id])
    render json: @category, include: :posts
  end
end
