class Api::CommentsController < ApplicationController

  def index
    if params[:filter]
      @comments = Comment.includes(:replies, :parent).where(id: params[:filter][:id].split(','))
      render json: @comments, include: ["replies"]
    else
      render json: [], status: :unprocessable_entity
    end
  end

  def show
    @comment = Comment.find(params[:id])
    render json: @comment
  end

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render :json => @comment, status: :ok
    else
      render json: @comment, status: :unprocessable_entity
    end
  end

  private

  def comment_params
    # parse_params = ActiveModelSerializers::Deserialization.jsonapi_parse(params)
    params.require(:data).require(:attributes).permit(:id, :author, :content, :post_id, :parent_id)
  end

end
