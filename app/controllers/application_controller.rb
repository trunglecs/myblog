class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def underscore_key(k)
    k.to_s.underscore.to_sym
  end

  def convert_hash_keys(value)
    case value
      when Array
        value.map { |v| convert_hash_keys(v) }
        # or `value.map(&method(:convert_hash_keys))`
      when Hash
        Hash[value.map { |k, v| [underscore_key(k), convert_hash_keys(v)] }]
      else
        value
     end
  end

end
